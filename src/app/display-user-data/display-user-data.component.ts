import { Component, OnInit } from '@angular/core';
import { UserInfoModel } from '../models/UserInfoModel';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-display-user-data',
  templateUrl: './display-user-data.component.html',
  styleUrls: ['./display-user-data.component.scss']
})
export class DisplayUserDataComponent implements OnInit {

    user: UserInfoModel = new UserInfoModel({guid: 'D21ds12x',
      customerUid: 'cust2dsa12dsa',
      first_name: 'John',
      last_name: 'Doe',
      email: 'email@email.com',
      zipcode: 10283,
      password: 'Idasn2x2#'});
    guid: string;
    submitted = false;
    userForm: FormGroup;
    serviceErrors: any = {};
    registered = false;

  constructor(private http: HttpClient, private router: ActivatedRoute) {
      this.http.get('/api/v1/generate_uid').subscribe((data: any) => {
        this.guid = data.guid;
      }, error => {
          console.log('There was an error generating the proper GUID on the server', error);
      });
  }

  private subscriber: any;
  ngOnInit() {
    this.subscriber = this.router.params.subscribe(param => {
      this.http.get('api/v1/customer/' + param.uid).subscribe((data: any) => {
        if (data) {
          this.user = new UserInfoModel(data.customer);
        }
      });
    });

  }
}
