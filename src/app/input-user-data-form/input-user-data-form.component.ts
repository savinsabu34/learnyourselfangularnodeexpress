import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input-user-data-form',
  templateUrl: './input-user-data-form.component.html',
  styleUrls: ['./input-user-data-form.component.scss']
})
export class InputUserDataFormComponent implements OnInit {

    registered = false;
    submitted = false;
    userForm: FormGroup;
    guid: string;
    serviceErrors: any = {};

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) {

    this.http.get('/api/v1/generate_uid').subscribe((data: any) => {
      this.guid = data.guid;
    }, error => {
        console.log('There was an error generating the proper GUID on the server', error);
    });
   }

    invalidFirstName() {
      return (this.submitted && (this.serviceErrors.firstName != null || this.userForm.controls.firstName.errors != null));
    }

    invalidLastName() {
      return (this.submitted && (this.serviceErrors.lastName != null || this.userForm.controls.lastName.errors != null));
    }

    invalidEmail() {
      return (this.submitted && (this.serviceErrors.email != null || this.userForm.controls.email.errors != null));
    }

    invalidZipcode() {
      return (this.submitted && (this.serviceErrors.zipcode != null || this.userForm.controls.zipcode.errors != null));
    }

    invalidPassword() {
      return (this.submitted && (this.serviceErrors.password != null || this.userForm.controls.password.errors != null));
    }
  ngOnInit() {
    this.userForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        zipcode: ['', [Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]],
        password: ['', [Validators.required, Validators.minLength(5),
                        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]],
      });
  }
  onSubmit() {
      this.submitted = true;

      if (this.userForm.invalid === true) {
        return;
      } else {
        let data: any = Object.assign({guid: this.guid}, this.userForm.value);
        this.http.post('api/v1/customer', data).subscribe((res: any) => {
            if (res) {
              console.log(res);
            } else {
              console.log('no');
            }
            let path = '/user/' + data.customer.uid;
            this.router.navigate([path]);
        }, error => {
          this.serviceErrors = error.error.error;
        });
        this.registered = true;
      }
    }

}
